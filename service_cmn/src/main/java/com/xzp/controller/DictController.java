package com.xzp.controller;

import com.xzp.model.dict.Dict;
import com.xzp.result.Result;
import com.xzp.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/10/29 16:54
 * @Version 1.0
 */
@RestController
@RequestMapping("/admin/cmn")
@Api("数据字典")
public class DictController {

    @Autowired
    private DictService dictService;

    @GetMapping
    @ApiOperation("得到数据字典")
    public Result getchildren(Long id){
       List<Dict> dicts= dictService.findchildren(id);
        return Result.success(dicts);
    }

    @GetMapping("/export")
    @ApiOperation("导出数据")
    public void export(HttpServletResponse response){
        dictService.exportexcel(response);
    }

    @PostMapping
    @ApiOperation("导入数据")
    public Result importdict(MultipartFile file){
        dictService.importEx(file);
        return Result.success();
    }

    @GetMapping("/{value}")
    @ApiOperation("根据value查询name")
    public String getNameByvalue(@PathVariable Integer value){
        String name= dictService.findNameByValue(value);
        return name;
    }

    @GetMapping("/crawn")
    @ApiOperation("根据value查询crawn")
    public boolean crawnOrNot(@RequestParam("value") String value){
      boolean b= dictService.crownOrNot(Integer.valueOf(value));
      return b;
    }
}
