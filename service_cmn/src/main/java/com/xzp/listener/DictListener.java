package com.xzp.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.xzp.mapper.DictMapper;
import com.xzp.model.dict.Dict;
import com.xzp.vo.cmn.DictExcelVo;
import org.springframework.beans.BeanUtils;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/10/30 16:49
 * @Version 1.0
 */
public class DictListener extends AnalysisEventListener<DictExcelVo> {

    public DictListener(DictMapper dictMapper) {
        this.dictMapper = dictMapper;
    }
    private DictMapper dictMapper;
    @Override
    public void invoke(DictExcelVo dictExcelVo, AnalysisContext analysisContext) {
        Dict dict = new Dict();
        BeanUtils.copyProperties(dictExcelVo,dict);
        dictMapper.insert(dict);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
    }
}
