package com.xzp.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xzp.listener.DictListener;
import com.xzp.mapper.DictMapper;
import com.xzp.model.dict.Dict;
import com.xzp.service.DictService;
import com.xzp.vo.cmn.DictExcelVo;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/10/29 16:31
 * @Version 1.0
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements DictService {

    @Autowired
    private DictMapper mapper;

    @Override
    public List<Dict> findchildren(Long id) {

        LambdaQueryWrapper<Dict> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Dict::getParentId,id);
        List<Dict> dicts = mapper.selectList(wrapper);
        dicts= dicts.stream().map((item)->{
            boolean b = haschildren(item.getId());
            item.setHasChildren(b);
            return item;
        }).collect(Collectors.toList());

        return dicts; }

    public boolean haschildren(Long id){
        LambdaQueryWrapper<Dict> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Dict::getParentId,id);
        Long count = mapper.selectCount(wrapper);
        return count>0;
    }

    //导出数据
    @Override
    public void exportexcel(HttpServletResponse response) {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("Utf-8");
        String filename="dict";
        response.setHeader("Content-disposition","attachment;filename="+filename+".xlsx");

        List<Dict> dicts = mapper.selectList(null);
        List<DictExcelVo> voArrayList = new ArrayList<>();
        voArrayList=dicts.stream().map((item)->{
            DictExcelVo excelVo = new DictExcelVo();
            BeanUtils.copyProperties(item,excelVo);
            return excelVo;
        }).collect(Collectors.toList());
        try {
            EasyExcel.write(response.getOutputStream(),DictExcelVo.class).sheet("dict").doWrite(voArrayList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //导入excel数据
    @Override
    public void importEx(MultipartFile file) {
        try {
            EasyExcel.read(file.getInputStream(),DictExcelVo.class,new DictListener(baseMapper)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String findNameByValue(Integer value) {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        if(value.equals("")){
            return "";
        }
        wrapper.eq(!StringUtils.isEmpty(value),"value",value);
        Dict dict = mapper.selectOne(wrapper);
        return dict.getName();
    }

    @Override
    public boolean crownOrNot(Integer value) {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        if(value.equals("") || value==null){
            return false;
        }
        String dictCode = mapper.selectById(value).getDictCode();

        if(dictCode==null || dictCode.equals("")){
            return false;
        }
        //bug dictcode=null 不能和“”比较，报错
        if(dictCode.equals("crown")){
            return true;
        }
        return false;
    }
}
