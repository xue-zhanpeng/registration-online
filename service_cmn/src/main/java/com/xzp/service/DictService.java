package com.xzp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xzp.model.dict.Dict;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/10/29 16:31
 * @Version 1.0
 */
public interface DictService extends IService<Dict> {
    List<Dict> findchildren(Long id);

    void exportexcel(HttpServletResponse response);

    void importEx(MultipartFile file);

    String findNameByValue(Integer value);

    boolean crownOrNot(Integer value);

}
