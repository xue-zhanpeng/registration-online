package com.xzp.hospital.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/1 15:36
 * @Version 1.0
 */
@ConfigurationProperties(
        prefix = "hosp"
)
@Data
public class HosptialProper {
    private Long id;

}
