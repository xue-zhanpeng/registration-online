package com.xzp.controller;

import com.alibaba.fastjson.JSONObject;
import com.xzp.model.Schedule;
import com.xzp.result.Result;
import com.xzp.result.ResultCode;
import com.xzp.service.DepartService;
import com.xzp.service.ScheduleService;
import com.xzp.util.YyghException;
import com.xzp.vo.depart.DepartmentVo;
import com.xzp.vo.hosp.ScheduleQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/4 15:04
 * @Version 1.0
 */
@RestController
@RequestMapping("/admin/hosp/depart")
@Api(tags = "科室和排班接口")
//@CrossOrigin(maxAge = 3600)
public class DepartAndScheduleController {

    @Autowired
    private DepartService service;

    @Autowired
    private ScheduleService scheduleService;

    @ApiOperation("根据医院编号查询科室")
    @GetMapping("/{hoscode}")
    public Result getdepart(@PathVariable String hoscode){
        List<DepartmentVo> departmentVoList=service.getDepartVolist(hoscode);
        return Result.success(departmentVoList);
    }

    @GetMapping("/schedulerule/{page}/{limit}")
    @ApiOperation("排班预约规则")
    public Result scheduleRule(@PathVariable Integer page,
                               @PathVariable Integer limit,
                               String hoscode,
                               String depcode){
        Map<String,Object> map = scheduleService.getScheduleRule(page,limit,hoscode,depcode);
        return Result.success(map);
    }

    @PostMapping
    @ApiOperation("根据医院编号，科室编号，日期查询排班")
    public Result getScheduleByHoscodeAndDepcode(@RequestBody ScheduleQueryVo scheduleQueryVo) throws ParseException {
        Map<String,Object> map=scheduleService.getScheduleByDepcodeAndTime(scheduleQueryVo);
        return Result.success(map);
    }

    @GetMapping("/schedule/{scheduleId}")
    @ApiOperation("根据排班ID查询mongoDb")
    public Result selectScheduleById(@PathVariable String scheduleId){
        Schedule schedule= scheduleService.getScheduleById(scheduleId);
        return Result.success(schedule);
    }

    @ApiOperation("更新Schedule")
    @PostMapping("/update")
    public Result updateScheduleById(@RequestBody Map<String,Object> requestMap){
        Map<String,Object> scheduledata= (Map<String, Object>) requestMap.get("schedule");
        Schedule schedule = JSONObject.parseObject(JSONObject.toJSONString(scheduledata), Schedule.class);
        if(schedule==null){
            throw new YyghException(ResultCode.PARAM_ERROR);
        }
        schedule.setUpdateTime(new Date());
        scheduleService.updateSchedule(schedule);
        return Result.success();
    }


}
