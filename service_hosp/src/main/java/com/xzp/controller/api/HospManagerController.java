package com.xzp.controller.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xzp.model.Department;
import com.xzp.model.Schedule;
import com.xzp.model.hosp.HospitalSet;
import com.xzp.model.hosp.Hosptial;
import com.xzp.result.ResultCode;
import com.xzp.service.DepartService;
import com.xzp.service.HospDetailService;
import com.xzp.service.HospManagerService;
import com.xzp.service.ScheduleService;
import com.xzp.util.HttpRequestHelper;
import com.xzp.util.MD5;
import com.xzp.util.YyghException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import com.xzp.result.Result;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/1 9:36
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/hosp")
@CrossOrigin
@Api(tags = "提供给医院的医院管理接口")
public class HospManagerController {

    @Autowired
    private HospManagerService service;

    @Autowired
    private DepartService departService;

    @Autowired
    private HospDetailService detailService;

    @Autowired
    private ScheduleService scheduleService;

    @PostMapping("/saveHospital")
    @ApiOperation("新增医院基本信息")
    public Result save(HttpServletRequest request){
        Map<String, Object> objectMap = signvalidation(request);
        service.save(objectMap);
        return Result.success();
    }

    @PostMapping("/hospital/show")
    @ApiOperation("查询医院信息")
    public Result show(HttpServletRequest request){
        Map<String, Object> map = signvalidation(request);
        Hosptial hosptial=  service.showhosp(map);
        return Result.success(hosptial);
    }

    @PostMapping("/saveDepartment")
    @ApiOperation("添加科室")
    public Result savedepartment(HttpServletRequest request){
        Map<String, Object> map = signvalidation(request);
        departService.savedepartment(map);
        return Result.success();
    }

    @PostMapping("/department/list")
    @ApiOperation("分页查询科室")
    public Result listdepart(HttpServletRequest request){
        Map<String, Object> map =signvalidation(request);
        Page<Department> model= departService.pagemodel(map);
        return Result.success(model);
    }

    @PostMapping("/department/remove")
    @ApiOperation("删除科室")
    public Result removedepart(HttpServletRequest request){
        Map map = signvalidation(request);
        departService.remove(map);
        return
    Result.success();
    }


    @PostMapping("/saveSchedule")
    @ApiOperation("添加排班")
    public Result saveschedule(HttpServletRequest request){
        Map<String,Object> map = signvalidation(request);
        scheduleService.save(map);
        return Result.success();
    }

    @PostMapping("/schedule/list")
    @ApiOperation("显示排班信息")
    public Result listschedule(HttpServletRequest request){
        Map map = signvalidation(request);
        Page<Schedule> schedules= scheduleService.listByhoscode(map);
        return Result.success(schedules);
    }

    public Map signvalidation(HttpServletRequest request){
        Map<String, String[]> parameterMap = request.getParameterMap();
        Map<String, Object> map = HttpRequestHelper.switchMap(parameterMap);
        LambdaQueryWrapper<HospitalSet> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(HospitalSet::getHoscode,map.get("hoscode"));
        String signKey = detailService.getOne(wrapper).getSignKey();
        String s = MD5.encrypt(signKey);
        if(!s.equals(map.get("sign"))){
            throw new YyghException(ResultCode.SIGN_ERROR);
        }
        return map;
    }

    @PostMapping("/schedule/remove")
    @ApiOperation("删除排班情况")
    public Result removeschedule(HttpServletRequest request){
        Map map = signvalidation(request);
        scheduleService.remove(map);
        return Result.success();
    }

}
