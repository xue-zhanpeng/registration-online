package com.xzp.controller.api;

import com.xzp.result.Result;
import com.xzp.service.HospManagerService;
import com.xzp.service.ScheduleService;
import com.xzp.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/14 15:47
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/hospapi")
@Api(tags = "前端科室相关")
public class HospApiController {
    @Autowired
    private ScheduleService scheduleService;
    @ApiOperation("获取可预约日期")
    @GetMapping("/scheduledate/{page}/{limit}")
    public Result getscheduleDate(@PathVariable("page")int page,
                                  @PathVariable("limit")int limit,
                                  String hoscode, String depcode){
        Map<String,Object> map=scheduleService.getScheduleDate(page,limit,hoscode,depcode);
        return Result.success(map);
    }
}
