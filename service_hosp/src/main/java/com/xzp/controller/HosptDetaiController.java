package com.xzp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xzp.model.hosp.HospitalSet;
import com.xzp.result.Result;
import com.xzp.service.HospDetailService;
import com.xzp.validat.SaveGroup;
import com.xzp.validat.UpdateGroup;
import com.xzp.vo.hosp.HospitalSetQueryVo;
import com.xzp.vo.hosp.HospitalStatusVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/10/25 17:31
 * @Version 1.0
 */
@RestController
@RequestMapping("/admin/hosp")
@Api(tags = "医院相关接口")
//@CrossOrigin
public class HosptDetaiController {

    @Autowired
    private HospDetailService service;

    @GetMapping
    @ApiOperation("获取所有医院信息")
    public Result getall(){
        List<HospitalSet> list = service.list();
        return Result.success(list);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除医院信息")
    @ApiParam(required = true)
    public Result delete(@PathVariable Long id){
        boolean b = service.removeById(id);
        if(b){
            return Result.success();
        }
        return Result.fail();
    }

    @PostMapping("/{page}/{pageSize}")
    @ApiOperation("根据条件分页查询")
    public Result querypage(@PathVariable("page") Integer page,
                             @PathVariable("pageSize") Integer pageSize,
                           @RequestBody HospitalSetQueryVo queryVo
                           ){
        QueryWrapper<HospitalSet> wrapper = new QueryWrapper<>();
        wrapper.like(queryVo.getHosname()!=null,"hosname",queryVo.getHosname());
        wrapper.like(queryVo.getHoscode()!=null,"hoscode",queryVo.getHoscode());
        Page<HospitalSet> page1 = service.page(new Page<>(page, pageSize), wrapper);
        return Result.success(page1);
    }

    @PostMapping
    @ApiOperation("新增医院信息")
    public  Result save(@RequestBody @Validated(SaveGroup.class) HospitalSet hospitalSet){
        UUID uuid = UUID.randomUUID();
        hospitalSet.setSignKey(uuid.toString());
        boolean save = service.save(hospitalSet);
        if(save){
            return Result.success();
        }
        return Result.fail();
    }

    @ApiOperation("根据id查询")
    @GetMapping("/{id}")
    public Result getByid(@PathVariable Long id){
        HospitalSet hospitalSet = service.getById(id);
        if(hospitalSet!=null){
        return Result.success(hospitalSet);}
        return Result.fail();
    }

    @GetMapping("/findByhoscode/{hoscode}")
    @ApiOperation("根据医院编号获取医院")
    public Result getHosByHoscode(@PathVariable String hoscode){
       HospitalSet hospitalSet=  service.getHospByHoscode(hoscode);
       return Result.success(hospitalSet);
    }

    @PutMapping
    @ApiOperation("更新医院信息")
    public Result update(@RequestBody @Validated(UpdateGroup.class) HospitalSet hospitalSet){
        boolean b = service.updateById(hospitalSet);
        if(b){
            return Result.success();
        }
        return Result.fail();
    }

    @ApiOperation("对医院加锁或者解锁")
    @PostMapping("/status")
    public Result changestatus(@RequestBody @Validated HospitalStatusVo statusVo){
        HospitalSet hospitalSet = service.getById(statusVo.getId());
        hospitalSet.setStatus(statusVo.getStatus());
        boolean b = service.updateById(hospitalSet);
        if(b){
            return Result.success();
        }
       return Result.fail();
    }
    @GetMapping("/signkey")
    @ApiOperation("发送秘钥")
    public Result getkey(Long id){
        HospitalSet id1 = service.getById(id);
        String signKey = id1.getSignKey();
        String hoscode = id1.getHoscode();
        HashMap<String, String> map = new HashMap<>();
        map.put(hoscode,signKey);
        //TODO 发送秘钥

        return Result.success(map);
    }
}
