package com.xzp.controller;

import com.xzp.model.hosp.Hosptial;
import com.xzp.result.Result;
import com.xzp.service.HospManagerService;
import com.xzp.vo.hosp.HospitalQueryVo;
import com.xzp.vo.order.DataShowVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/2 15:37
 * @Version 1.0
 */
@RestController
@RequestMapping("/admin/hosp/mongohosp")
@Api(tags = "医院Mongodb后台接口")
//@CrossOrigin
public class HospitalShowController {


    @Autowired
    private RedisTemplate redisTemplate;

    private static String VIEWS_KEY="views:";

    @Autowired
    private HospManagerService service;

    @GetMapping("/{page}/{limit}")
    @ApiOperation("查询mongo医院信息")
    public Result showmongohosp(@PathVariable(value = "page") Integer strpage,
                                @PathVariable(value = "limit") Integer strlimit,
                                HospitalQueryVo queryVo
                                ){
        Page<Hosptial> hosptials= service.findall(strpage,strlimit,queryVo);
        return Result.success(hosptials);
    }

    @GetMapping("/status/{id}/{status}")
    @ApiOperation("更改医院状态")
    public Result updatestatus(@PathVariable String id,
                               @PathVariable int status){
        service.updatestatus(id,status);
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("查看医院详情")
    public Result showhospital(@PathVariable String id){
       Hosptial result= service.getoneByid(id);
        return Result.success(result);
    }

    @ApiOperation("根据医院名称模糊查询")
    @PostMapping
    public Result likeAsnc(@RequestBody Hosptial hosptial){
        List<Hosptial> result=service.likeByasnc(hosptial);
        return Result.success(result);
    }

    @ApiOperation("根据医院编号查询详情")
    @GetMapping("/byhoscode/{hoscode}")
    public Result getHospByHoscode(@PathVariable String hoscode,HttpServletRequest request){
        //TODO 会接收两次相同请求导致views加2
        Hosptial hosptial= service.getHospByHoscode(hoscode);
            String key=VIEWS_KEY+hoscode;
            Integer views=0;
            Object key1 = redisTemplate.opsForValue().get(key);
            if(key1==null){
                redisTemplate.opsForValue().set(key,1);
            }else {
                views = (Integer) key1;
                redisTemplate.opsForValue().set(key, views + 1);
            }
        return Result.success(hosptial);
    }

    @ApiOperation("得到医院列表，科室数量，排班数量map")
    @PostMapping("/hospdata")
    public Map<String,Object> hospDepartData(@RequestBody DataShowVo dataShowVo){
        Map<String,Object>  result=service.staticData(dataShowVo);
        return result;
    }
}
