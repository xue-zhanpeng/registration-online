package com.xzp.mqconsumer;

import com.rabbitmq.client.Channel;
import com.xzp.Enum.MqConstant;
import com.xzp.model.Schedule;
import com.xzp.model.order.Order;
import com.xzp.service.RabbitService;
import com.xzp.service.ScheduleService;
import com.xzp.vo.hosp.ScheduleMqVo;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/17 14:40
 * @Version 1.0
 */
@Component
public class HosptialReceiver {

    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private RabbitService rabbitService;

    /**
     * @Queue是绑定指定队列，rabbitmq会自动根据名称创建
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = MqConstant.QUEUE_ORDER,durable = "true"),
            exchange = @Exchange(value = MqConstant.EXCHANGE_DIRECT_ORDER),
            key = {MqConstant.ROUTING_ORDER}
    ))
    public void receiversave(ScheduleMqVo scheduleMqVo, Message message, Channel channel){
        Schedule schedule = scheduleService.getScheduleById(scheduleMqVo.getScheduleId());
        schedule.setAvailableNumber(scheduleMqVo.getAvailableNumber());
        schedule.setReservedNumber(scheduleMqVo.getReservedNumber());
        scheduleService.updateSchedule(schedule);
    }

//    @RabbitListener(bindings = @QueueBinding(
//            value = @Queue(value = MqConstant.QUEUE_CANCEL,durable = "true"),
//            exchange = @Exchange(value = MqConstant.EXCHANGE_DIRECT_ORDER),
//            key = {MqConstant.ROUTING_CANCEL}
//    ))
    public void receiverCancel(ScheduleMqVo scheduleMqVo,Message message,Channel channel){
        Schedule schedule = scheduleService.getScheduleById(scheduleMqVo.getScheduleId());
        schedule.setAvailableNumber(schedule.getAvailableNumber()+1);
        scheduleService.updateSchedule(schedule);
        String hosScheduleId = schedule.getHosScheduleId();

    }
}
