package com.xzp.mapper;

import com.xzp.model.Schedule;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/2 9:28
 * @Version 1.0
 */
public interface ScheduleMapper extends MongoRepository<Schedule,String> {
}
