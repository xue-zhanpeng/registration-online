package com.xzp.mapper;

import com.xzp.model.Department;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/1 16:05
 * @Version 1.0
 */
public interface DepartMapper extends MongoRepository<Department,String> {
}
