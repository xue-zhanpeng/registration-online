package com.xzp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xzp.model.hosp.HospitalSet;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/10/25 14:58
 * @Version 1.0
 */
public interface HospDetailMapper extends BaseMapper<HospitalSet> {
}
