package com.xzp.service;

import com.xzp.model.hosp.Hosptial;
import com.xzp.vo.hosp.HospitalQueryVo;
import com.xzp.vo.order.DataShowVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/1 11:23
 * @Version 1.0
 */
public interface HospManagerService {
    void save(Map<String, Object> objectMap);

    Hosptial showhosp(Map<String, Object> map);

    Page<Hosptial> findall(Integer strpage, Integer strlimit, HospitalQueryVo queryVo);

    void updatestatus(String id, int status);

    Hosptial getoneByid(String id);

    List<Hosptial> likeByasnc(Hosptial hosptial);

    Hosptial getHospByHoscode(String hoscode);

    Map<String, Object> staticData(DataShowVo dataShowVo);

}
