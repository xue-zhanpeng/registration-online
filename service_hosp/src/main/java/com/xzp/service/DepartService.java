package com.xzp.service;

import com.xzp.model.Department;
import com.xzp.vo.depart.DepartmentVo;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Map;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/1 16:06
 * @Version 1.0
 */
public interface DepartService {
    void savedepartment(Map<String, Object> map);

    Page<Department> pagemodel(Map<String, Object> map);

    void remove(Map map);

    List<DepartmentVo> getDepartVolist(String hoscode);

    Department getdepartBydepcode(String hoscode, String depcode);
}
