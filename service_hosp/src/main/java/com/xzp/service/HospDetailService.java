package com.xzp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xzp.model.hosp.HospitalSet;
import com.xzp.model.hosp.Hosptial;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/10/25 15:00
 * @Version 1.0
 */
public interface HospDetailService  extends IService<HospitalSet> {
    HospitalSet getHospByHoscode(String hoscode);

}
