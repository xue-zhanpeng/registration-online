package com.xzp.service;

import com.xzp.model.Schedule;
import com.xzp.vo.hosp.ScheduleQueryVo;
import org.springframework.data.domain.Page;

import java.text.ParseException;
import java.util.Map;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/2 9:29
 * @Version 1.0
 */
public interface ScheduleService {
    void save(Map<String, Object> map);

    Page<Schedule> listByhoscode(Map map);

    void remove(Map map);

    Map<String, Object> getScheduleRule(Integer page, Integer limit, String hoscode, String depcode);

    Map<String, Object> getScheduleByDepcodeAndTime(ScheduleQueryVo scheduleQueryVo) throws ParseException;

    Map<String, Object> getScheduleDate(int page, int limit, String hoscode, String depcode);

    Schedule getScheduleById(String scheduleId);

    boolean updateSchedule(Schedule schedule);

    Integer getscheduleCount(String hoscode);

}
