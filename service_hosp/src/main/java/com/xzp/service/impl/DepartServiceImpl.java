package com.xzp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xzp.mapper.DepartMapper;
import com.xzp.model.Department;
import com.xzp.model.hosp.HospitalSet;
import com.xzp.result.ResultCode;
import com.xzp.service.DepartService;
import com.xzp.service.HospDetailService;
import com.xzp.util.MD5;
import com.xzp.util.YyghException;
import com.xzp.vo.depart.DepartmentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/1 16:07
 * @Version 1.0
 */
@Service
public class DepartServiceImpl  implements DepartService {

    @Autowired
    private DepartMapper mapper;

    @Autowired
    private HospDetailService detailService;

    @Override
    public void savedepartment(Map<String, Object> map) {
        Department department = JSONObject.parseObject(JSONObject.toJSONString(map), Department.class);
        Department department2 = this.getDepartmentBYDepcode(department.getDepcode());
        if(department2!=null){
            //科室存在就更新
            department.setCreateTime(department2.getCreateTime());
            department.setIsDeleted(0);
            department.setUpdateTime(new Date());
            mapper.save(department);
        }else {
            //科室不存在，新增
            department.setUpdateTime(new Date());
            department.setIsDeleted(0);
            department.setCreateTime(new Date());
            mapper.save(department);
        }
    }

    public Department getDepartmentBYDepcode(String depcode){
        Department department = new Department();
        department.setDepcode(depcode);
        Example<Department> of = Example.of(department);
        boolean exists = mapper.exists(of);
        if(exists){
            Department department1 = mapper.findOne(of).get();
            return department1;
        }
        return null;
    }

    @Override
    public Page<Department> pagemodel(Map<String, Object> map) {
        LambdaQueryWrapper<HospitalSet> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(HospitalSet::getHoscode,map.get("hoscode"));
        String signKey = detailService.getOne(wrapper).getSignKey();
        String s = MD5.encrypt(signKey);
        if(!s.equals(map.get("sign"))){
            throw new YyghException(ResultCode.SIGN_ERROR);
        }
        Integer page = StringUtils.isEmpty(map.get("page"))?1:Integer.parseInt((String) map.get("page"));
        Integer limit = StringUtils.isEmpty(map.get("limit"))?5:Integer.parseInt((String) map.get("limit"));

        Department department = new Department();
        department.setHoscode((String) map.get("hoscode"));
        department.setIsDeleted(0);
        Example<Department> example = Example.of(department);

        //页数从0开始
        Pageable pageable= PageRequest.of(page-1,limit);
        Page<Department> page1 = mapper.findAll(example, pageable);
        return page1;
    }

    @Override
    public void remove(Map map) {
        String hoscode = (String) map.get("hoscode");
        String depcode = (String) map.get("depcode");
        Department department = new Department();
        department.setHoscode(hoscode);
        department.setDepcode(depcode);
        Example<Department> example = Example.of(department);
        boolean exists = mapper.exists(example);
        if(!exists){
           throw new YyghException(ResultCode.FETCH_USERINFO_ERROR);
        }
        Department department1 = mapper.findOne(example).get();
        department1.setIsDeleted(1);
        department1.setUpdateTime(new Date());
        mapper.save(department1);
    }

    @Override
    public List<DepartmentVo> getDepartVolist(String hoscode) {
        List<DepartmentVo> list = new ArrayList<>();
        //查询所有科室
        Department department = new Department();
        department.setHoscode(hoscode);
        Example<Department> example = Example.of(department);
        List<Department> alldepartment = mapper.findAll(example);
        //根据bigcode对科室分类,使用stream流得到map对象，key为分类的字段，value为分类下的列表
        Map<String, List<Department>> listMap = alldepartment.stream().collect(Collectors.groupingBy(Department::getBigcode));
        for (Map.Entry<String,List<Department>> entry:listMap.entrySet()){
            //遍历map对外层封装
            String bigcode = entry.getKey();
            DepartmentVo departmentVo = new DepartmentVo();
            departmentVo.setDepcode(bigcode);
            List<Department> departments = entry.getValue();
            departmentVo.setDepname(departments.get(0).getBigname());
            //封装children
           List<DepartmentVo> childrenList = new ArrayList<>();
           departments.stream().forEach(item->{
               DepartmentVo childrenVo = new DepartmentVo();
               childrenVo.setDepname(item.getDepname());
               childrenVo.setDepcode(item.getDepcode());
               childrenList.add(childrenVo);
           });
           departmentVo.setChildren(childrenList);
           list.add(departmentVo);
        }
        return list;
    }

    @Override
    public Department getdepartBydepcode(String hoscode, String depcode) {
        Department department = new Department();
        department.setHoscode(hoscode);
        department.setDepcode(depcode);
        Example<Department> example = Example.of(department);
        Optional<Department> one = mapper.findOne(example);
        if(one==null){
            return department;
        }
        return one.get();
    }
}
