package com.xzp.service.impl;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.excel.util.CollectionUtils;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xzp.mapper.ScheduleMapper;
import com.xzp.model.Schedule;
import com.xzp.model.hosp.BookingRule;
import com.xzp.model.hosp.Hosptial;
import com.xzp.result.ResultCode;
import com.xzp.service.DepartService;
import com.xzp.service.HospManagerService;
import com.xzp.service.ScheduleService;
import com.xzp.util.Time;
import com.xzp.util.YyghException;
import com.xzp.vo.hosp.HospStaticVo;
import com.xzp.vo.hosp.RuleScheduleVo;
import com.xzp.vo.hosp.ScheduleQueryVo;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/2 9:29
 * @Version 1.0
 */
@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleMapper mapper;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private HospManagerService hospManagerService;

    @Autowired
    private DepartService departService;

    @Override
    public void save(Map<String, Object> map) {
        String mapstring = JSONObject.toJSONString(map);
        Schedule schedule = JSONObject.parseObject(mapstring, Schedule.class);
        Schedule schedule1 = new Schedule();
        schedule1.setHosScheduleId(schedule.getHosScheduleId());
        Example<Schedule> of = Example.of(schedule1);
        if(mapper.findOne(of).isEmpty()){
            schedule.setCreateTime(new Date());
            schedule.setUpdateTime(new Date());
            schedule.setIsDeleted(0);
        }else {
            Schedule schedule2 = mapper.findOne(of).get();
            schedule.setCreateTime(schedule2.getCreateTime());
            schedule.setUpdateTime(new Date());
            schedule.setIsDeleted(schedule2.getIsDeleted());
        }
        mapper.save(schedule);
    }

    @Override
    public Page<Schedule> listByhoscode(Map map) {
       Integer page= StringUtils.isEmpty((String) map.get("page"))?1:Integer.parseInt((String) map.get("page"));
       Integer limit= StringUtils.isEmpty((String) map.get("limit"))?1:Integer.parseInt((String) map.get("limit"));
        String hoscode = (String) map.get("hoscode");
        Schedule schedule = new Schedule();
        schedule.setStatus(1);
        schedule.setHoscode(hoscode);
        schedule.setIsDeleted(0);
        Example<Schedule> example = Example.of(schedule);
        //从第0页开始的
        Pageable pageable = PageRequest.of(page-1, limit);
        Page<Schedule> all = mapper.findAll(example, pageable);
        return all;
    }

    @Override
    public void remove(Map map) {
        String hoscode =(String) map.get("hoscode");
        String hosscheduleId=(String) map.get("hosScheduleId");
        Schedule schedule = new Schedule();
        schedule.setHoscode(hoscode);
        schedule.setHosScheduleId(hosscheduleId);
        Example<Schedule> example = Example.of(schedule);
        if(!mapper.exists(example)){
            throw new YyghException(ResultCode.FETCH_USERINFO_ERROR);
        }
        Schedule schedule1 = mapper.findOne(example).get();
        schedule1.setIsDeleted(1);
        schedule1.setUpdateTime(new Date());
        mapper.save(schedule1);
    }

    @Override
    public Map<String, Object> getScheduleRule(Integer page, Integer limit, String hoscode, String depcode) {
        //查询所有排班
        //封装条件
        Criteria c=Criteria.where("hoscode").is(hoscode).and("depcode").is(depcode);
        Aggregation aggregation=Aggregation.newAggregation(
                Aggregation.match(c),
                Aggregation.group("workDate")
                .first("workDate").as("workDate")
                //统计数量
                .count().as("docCount")
                .sum("reservedNumber").as("reservedNumber")
                .sum("availableNumber").as("availableNumber"),
                Aggregation.sort(Sort.Direction.DESC,"workDate"),
                //分页
                Aggregation.skip((page-1)*limit),
                Aggregation.limit(limit)
        );
        AggregationResults<RuleScheduleVo> ruleScheduleVos = mongoTemplate.aggregate(aggregation, Schedule.class, RuleScheduleVo.class);
        List<RuleScheduleVo> results = ruleScheduleVos.getMappedResults();
        results.stream().forEach(item->{
            DateTime dateTime = new DateTime(item.getWorkDate());
            item.setWeekday(weekday(dateTime));
        });
        //对结果进行分组，求和，分页
        Map<String,Object> map=new HashMap<>();
        map.put("result",results);
        Aggregation agg=Aggregation.newAggregation(
                Aggregation.match(c),
                Aggregation.group("workDate")
        );
        int size = mongoTemplate.aggregate(agg, Schedule.class, RuleScheduleVo.class).getMappedResults().size();
        map.put("total",size);
        //TODO 使用mapper进行分组聚合
        return map;
    }


    @Override
    public Map<String, Object> getScheduleByDepcodeAndTime(ScheduleQueryVo scheduleQueryVo) throws ParseException {
        Map<String,Object> result= new HashMap<>();
        Schedule schedule = new Schedule();
        schedule.setDepcode(scheduleQueryVo.getDepcode());
        schedule.setHoscode(scheduleQueryVo.getHoscode());
        Date scheduledate = DateTime.parse(scheduleQueryVo.getWorkDate()).toDate();
        schedule.setWorkDate(scheduledate);
        ExampleMatcher matche=ExampleMatcher.matching().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        Example<Schedule> example = Example.of(schedule,matche);
        List<Schedule> all = mapper.findAll(example);
        DateTime dateTime = new DateTime(scheduleQueryVo.getWorkDate());
        String weekday = weekday(dateTime);
        all.stream().forEach(item->{
            item.getParam().put("weekday",weekday);
        });
        result.put("result",all);
        return result;
    }

    /**
     * 获取当前日期以后的可预约日期
     * @param page
     * @param limit
     * @param hoscode
     * @param depcode
     * @return
     */
    @Override
    public Map<String, Object> getScheduleDate(int page, int limit, String hoscode, String depcode) {
        Map<String,Object> result= new HashMap<>();
//        获取预约规则
        Hosptial hosptial = hospManagerService.getHospByHoscode(hoscode);
        if(hosptial==null){
            throw new YyghException(ResultCode.DATA_ERROR);
        }
        BookingRule bookingRule = hosptial.getBookingRule();
//      通过预约规则得到可预约日期,cycle预约周期就是放号的天数
//      判断是否过了放号时间
        DateTime currReleaseTime = Time.changeToDate(new Date(), bookingRule.getReleaseTime());
        Integer cycle = bookingRule.getCycle();//预约周期
//      isBeforeNow()判断放号时间是否在当前时间前
        if(currReleaseTime.isBeforeNow()){
            cycle=cycle+1;
        }
//      根据周期得到日期集合
        List<Date> dateList=new ArrayList<>();
        for (int i = 0; i < cycle; i++) {
            DateTime nextdateTime = currReleaseTime.plusDays(i);
            String format = nextdateTime.toString("yyyy-MM-dd");
            dateList.add(new DateTime(format).toDate());
        }
//        判断日期是否超过七天，要进行分页
        int end=(page-1)*limit+limit;
        int start=(page-1)*limit;
        if(end>dateList.size()){
            end=dateList.size();
        }
//      得到页面显示的日期集合
        List<Date> pageDatelist= new ArrayList<>();
        for (int i = start; i <end; i++) {
            pageDatelist.add(dateList.get(i));
        }
        IPage<Date> page1=new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(page,7,dateList.size());
        page1.setRecords(pageDatelist);
//        封装查询条件
        Criteria criteria=Criteria.where("hoscode").is(hoscode).and("depcode").is(depcode).and("workDate").in(pageDatelist);
        Aggregation aggregation=Aggregation.newAggregation(
                Aggregation.match(criteria),
                Aggregation.group("workDate").first("workDate").as("workDate")
                .count().as("docCount")
                .sum("reservedNumber").as("reservedNumber")
                .sum("availableNumber").as("availableNumber")
        );
        AggregationResults<RuleScheduleVo> voAggregationResults = mongoTemplate.aggregate(aggregation, Schedule.class, RuleScheduleVo.class);
        List<RuleScheduleVo> mappedResults = voAggregationResults.getMappedResults();
//        对日期进行数据合并
        Map<Date,RuleScheduleVo> mergeMap=new HashMap<>();
        if(!CollectionUtils.isEmpty(mappedResults)){
             mergeMap = mappedResults.stream().collect(Collectors.toMap(RuleScheduleVo::getWorkDate, RuleScheduleVo -> RuleScheduleVo));
        }
        List<RuleScheduleVo> finalRulevoList =  new ArrayList<>();//最后的结果集合
        for (int i = start; i < end; i++) {
            Date date=dateList.get(i);
            RuleScheduleVo scheduleVo = mergeMap.get(date);
            if(scheduleVo==null){
                //当天没有医生排班
                scheduleVo=new RuleScheduleVo();
                scheduleVo.setDocCount(0);
                scheduleVo.setAvailableNumber(-1);
            }
            scheduleVo.setWorkDate(date);
            scheduleVo.setWeekday(weekday(new DateTime(date)));
            //最后一条纪录要显示即将放号，不能预约 1:即将放号 0：正常
            if(i==dateList.size()-1 && page==page1.getPages()){
                scheduleVo.setStatus(1);
            }else {
                scheduleVo.setStatus(0);
            }
            //判断当天是否超过停号时间
            if(i==0 && page==1){
                DateTime stoptime = Time.changeToDate(new Date(), bookingRule.getStopTime());
                if(stoptime.isBeforeNow()){
            //  停止预约
                    scheduleVo.setStatus(-1);
                }
            }
            finalRulevoList.add(scheduleVo);
        }
        result.put("ruleschedule",finalRulevoList);
        result.put("total",page1.getTotal());
        result.put("NowDate",new DateTime().toString("yyyy年MM月"));
        result.put("hosname",hosptial.getHosname());
        result.put("depname",departService.getdepartBydepcode(hoscode,depcode).getDepname());
        result.put("releaseTime",bookingRule.getReleaseTime());
        result.put("bigname",departService.getdepartBydepcode(hoscode,depcode).getBigname());
        result.put("stopTime",bookingRule.getStopTime());
        return result;
    }

    @Override
    public Schedule getScheduleById(String scheduleId) {
        Optional<Schedule> optional = mapper.findById(scheduleId);
        if(optional.isEmpty()){
            throw new YyghException(ResultCode.DATA_ERROR);
        }
        Schedule schedule = optional.get();
        Hosptial hosptial = hospManagerService.getHospByHoscode(schedule.getHoscode());
        schedule.getParam().put("hosname",hosptial.getHosname());
        schedule.getParam().put("depname",departService.getdepartBydepcode(schedule.getHoscode(),schedule.getDepcode()).getDepname());
        return schedule;
    }

    @Override
    public boolean updateSchedule(Schedule schedule) {
        schedule.setUpdateTime(new Date());
        mapper.save(schedule);
        return true;
    }

    @Override
    public Integer getscheduleCount(String hoscode) {
        Criteria criteria=Criteria.where("hoscode").is(hoscode).and("workDate").gt(new DateTime().toDate());
        Aggregation aggregation=Aggregation.newAggregation(
          Aggregation.match(criteria),
          Aggregation.count().as("Count")
        );
        AggregationResults<HospStaticVo> aggregate = mongoTemplate.aggregate(aggregation, Schedule.class, HospStaticVo.class);
        List<HospStaticVo> results = aggregate.getMappedResults();
        if(results.size()==0){
            return 0;
        }
        HospStaticVo hospStaticVo = results.get(0);
        if(hospStaticVo!=null){
            return hospStaticVo.getCount();
        }
        return 0;
    }

    private String weekday(@NotNull DateTime time){
        String weekday="";
        switch (time.getDayOfWeek()){
            case DateTimeConstants
                    .SUNDAY:
                weekday= "周日";
            break;
            case DateTimeConstants
                    .SATURDAY:
                weekday= "周六";
            break;
            case DateTimeConstants
                    .FRIDAY:
                weekday= "周五";
            break;
            case DateTimeConstants
                    .THURSDAY:
                weekday= "周四";
            break;
            case DateTimeConstants
                    .WEDNESDAY:
                weekday= "周三";
            break;
            case DateTimeConstants
                    .TUESDAY:
                weekday= "周二";
            break;
            case DateTimeConstants
                    .MONDAY:
                weekday= "周一";
            break;
        }
        return weekday;
    }

}
