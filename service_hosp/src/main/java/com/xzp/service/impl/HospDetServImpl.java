package com.xzp.service.impl;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xzp.mapper.HospDetailMapper;
import com.xzp.model.hosp.HospitalSet;
import com.xzp.model.hosp.Hosptial;
import com.xzp.result.ResultCode;
import com.xzp.service.HospDetailService;
import com.xzp.util.YyghException;
import org.springframework.stereotype.Service;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/10/25 15:02
 * @Version 1.0
 */
@Service
public class HospDetServImpl extends ServiceImpl<HospDetailMapper, HospitalSet> implements HospDetailService {


    @Override
    public HospitalSet getHospByHoscode(String hoscode) {
        LambdaQueryWrapper<HospitalSet> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(!StringUtils.isEmpty(hoscode),HospitalSet::getHoscode,hoscode);
        HospitalSet hospitalSet = baseMapper.selectOne(wrapper);
        if(hospitalSet==null){
            throw new YyghException(ResultCode.DATA_ERROR);
        }
        return hospitalSet;
    }
}
