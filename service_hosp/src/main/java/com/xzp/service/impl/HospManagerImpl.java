package com.xzp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.xzp.cmn.CmnClient;
import com.xzp.mapper.HospMongoMapper;

import com.xzp.model.Department;
import com.xzp.model.hosp.Hosptial;

import com.xzp.result.ResultCode;
import com.xzp.service.HospDetailService;
import com.xzp.service.HospManagerService;

import com.xzp.service.ScheduleService;
import com.xzp.util.YyghException;
import com.xzp.vo.hosp.HospStaticVo;
import com.xzp.vo.hosp.HospitalQueryVo;
import com.xzp.vo.order.DataShowVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 时间未到，资格未够，继续努力！
 *
 * @Author xuezhanpeng
 * @Date 2022/11/1 11:24
 * @Version 1.0
 */
@Service
public class HospManagerImpl implements HospManagerService {

    @Autowired
    private HospMongoMapper mapper;

    @Autowired
    private HospDetailService detailService;

    @Autowired
    private CmnClient cmnClient;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private RedisTemplate redisTemplate;

    private static String VIEWS_KEY="views:";
    /**
     * 新增前判断是否存在此对象
     *
     * @param objectMap
     */
    @Override
    public void save(Map<String, Object> objectMap) {
        String data = (String) objectMap.get("logoData");
        data=data.replaceAll(" ","+");
        objectMap.put("logoData",data);
//        转换为对象
        String jsonString = JSONObject.toJSONString(objectMap);
        Hosptial hosptial = JSONObject.parseObject(jsonString, Hosptial.class);
        Hosptial hosptial1 = new Hosptial();
        hosptial1.setHoscode(hosptial.getHoscode());
        Example<Hosptial> example = Example.of(hosptial1);
        boolean exists = mapper.exists(example);
        if(exists){
            //存在就更新
            Hosptial oldhosp = mapper.findOne(example).get();
            hosptial.setCreateTime(oldhosp.getCreateTime());
            hosptial.setUpdateTime(new Date());
            hosptial.setStatus(oldhosp.getStatus());
            hosptial.setId(oldhosp.getId());
            hosptial.setIsDeleted(oldhosp.getIsDeleted());
            mapper.save(hosptial);
        }else {
            //不存在就新增
            hosptial.setIsDeleted(0);
            hosptial.setStatus(0);
            hosptial.setUpdateTime(new Date());
            hosptial.setCreateTime(new Date());
            mapper.save(hosptial);
        }
    }

    @Override
    public Hosptial showhosp(Map<String, Object> map) {
        Hosptial hosptial1 = new Hosptial();
        hosptial1.setHoscode((String) map.get("hoscode"));
        Example<Hosptial> example = Example.of(hosptial1);
        Hosptial hosptial = mapper.findOne(example).get();
        if(hosptial==null){
            return null;
        }
        return hosptial;
    }

    @Override
    public Page<Hosptial> findall(Integer page, Integer limit, HospitalQueryVo queryVo) {
        Pageable pageable = PageRequest.of(page - 1, limit);
        ExampleMatcher matcher = ExampleMatcher.matching().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING).withIgnoreCase();
        Hosptial hosptial = new Hosptial();
        BeanUtils.copyProperties(queryVo,hosptial);
        hosptial=crownset(hosptial);
        Example<Hosptial> example = Example.of(hosptial, matcher);
        Page<Hosptial> all = mapper.findAll(example, pageable);
        for (Hosptial item:all.getContent()) {
            Integer value = Integer.valueOf(item.getHostype());
            String hostypename = cmnClient.getNameByvalue(value);
            item.getParam().put("hoslevel", hostypename.replaceAll("\"",""));
            String provinceCode = item.getProvinceCode();
            String province = isempty(provinceCode);
            String citycode = item.getCityCode();
            String city = isempty(citycode);
            String distracode = item.getDistrictCode();
            String distract = isempty(distracode);
            String s = province + city + distract;
            item.getParam().put("address", s.replaceAll("\"",""));
        }
        return all;
    }

    @Override
    public void updatestatus(String id, int status) {
        Hosptial hosptial = mapper.findById(id).get();
        if(hosptial==null){
            throw new  YyghException(ResultCode.FETCH_USERINFO_ERROR);
        }
        hosptial.setStatus(status);
        hosptial.setUpdateTime(new Date());
        mapper.save(hosptial);
    }

    //查询医院详情
    @Override
    public Hosptial getoneByid(String id) {
        Hosptial hosptial = mapper.findById(id).get();
        if(hosptial==null){
            throw new YyghException(ResultCode.FETCH_USERINFO_ERROR);}
        hosptial.getParam().put("hoslevel",cmnClient.getNameByvalue(Integer.valueOf(hosptial.getHostype())));
        String province = isempty(hosptial.getProvinceCode());
        String city = isempty(hosptial.getCityCode());
        String distract = isempty(hosptial.getDistrictCode());
        String s = province + city + distract;
        hosptial.getParam().put("fulladdress",s.replaceAll("\"",""));
        return hosptial;
    }

    //判断是否是直辖市,并返回新的查询实体
    public Hosptial crownset(Hosptial hosp){
        if(hosp.getProvinceCode()==null || hosp.getProvinceCode().equals("")){
            return hosp;
        }
        boolean b = cmnClient.crawnOrNot(hosp.getProvinceCode());
        if(b){
            hosp.setCityCode(hosp.getProvinceCode());
            hosp.setProvinceCode("");
        }
        return hosp;
    }

    public String isempty(String Code){
        String address1 = null;
        //判断是否为空
        if(Code.equals("") || Code==null){
            address1=Code;
        }else {
            address1=cmnClient.getNameByvalue(Integer.valueOf(Code));
        }
        return address1;
    }

    //根据医院名称模糊查询
    @Override
    public List<Hosptial> likeByasnc(Hosptial hosptial) {
        ExampleMatcher matcher = ExampleMatcher.matching().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING).withIgnoreCase();
        Example<Hosptial> of = Example.of(hosptial, matcher);
        List<Hosptial> all = mapper.findAll(of);
        return all;
    }

    @Override
    public Hosptial getHospByHoscode(String hoscode) {
        Hosptial hosptial = new Hosptial();
        hosptial.setHoscode(hoscode);
        Example<Hosptial> example = Example.of(hosptial);
        Hosptial one = mapper.findOne(example).isEmpty()?null:mapper.findOne(example).get();
        if(one!=null){
            one.getParam().put("hoslevel",cmnClient.getNameByvalue(Integer.valueOf(one.getHostype())).replaceAll("\"",""));
        }
        return one;
    }

    @Override
    public Map<String, Object> staticData(DataShowVo dataShowVo) {
        Hosptial hosptial=new Hosptial();
        if(!dataShowVo.getHosname().equals("") || dataShowVo.getHosname()!=null){
            hosptial.setHosname(dataShowVo.getHosname());
        }
        ExampleMatcher matcher=ExampleMatcher.matching().withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING).withIgnoreCase();
        Example<Hosptial> example = Example.of(hosptial, matcher);

        List<Hosptial> all = mapper.findAll(example);
        List<String> hosnamelist=new ArrayList<>();
        Map<String,Object> resultmap=new HashMap<>();
        List<Integer> depCountlist=new ArrayList<>();
        List<Integer> schedulelist=new ArrayList<>();
        List<Integer> viewslist=new ArrayList<>();
        //遍历列表，得到map，key是医院名称，value是列表，列表中包含map当前医院的科室数量信息，排班数量信息，订单信息
        if(all.size()>0){
            all.stream().forEach(item->{
                hosnamelist.add(item.getHosname());
                Integer count=scheduleService.getscheduleCount(item.getHoscode());
                schedulelist.add(count);
                Criteria criteria=Criteria.where("hoscode").is(item.getHoscode());
                 Aggregation aggregation=Aggregation.newAggregation(
                 Aggregation.match(criteria),
                 Aggregation.count().as("Count")
                 );
                AggregationResults<HospStaticVo> aggregates = mongoTemplate.aggregate(aggregation, Department.class,HospStaticVo.class);
                List<HospStaticVo> mappedResults = aggregates.getMappedResults();
                mappedResults.stream().forEach(depone->{
                    depCountlist.add(depone.getCount());
                });
                String key=VIEWS_KEY+item.getHoscode();
                Object views = redisTemplate.opsForValue().get(key);
                Integer view= views==null?0:(Integer) views;
                viewslist.add(view);
            });
            if(hosnamelist.size()!=depCountlist.size()){
                int abs = Math.abs(hosnamelist.size() - depCountlist.size());
                for (int i = 0; i < abs; i++) {
                    depCountlist.add(0);
                }
            }
            resultmap.put("hoslist",hosnamelist);
            resultmap.put("departlist",depCountlist);
            resultmap.put("schedulelist",schedulelist);
            resultmap.put("viewslist",viewslist);
        }
        return resultmap;
    }
}
